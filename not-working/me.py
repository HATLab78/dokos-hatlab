# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# MIT License. See license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
import frappe.www.list

no_cache = 1


def get_context(context):
    if frappe.session.user=='Guest':
        frappe.throw(_("You need to be logged in to access this page"), frappe.PermissionError)


    # HATLAB Context
    context.user = frappe.get_doc("User", frappe.session.user)
    context.customer = frappe.get_doc("Customer", context.user.full_name)
    context.contact = frappe.get_doc("Contact", context.user.full_name)
    context.address = frappe.get_doc("Address", context.customer.customer_primary_address)