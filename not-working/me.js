frappe.ready(function() {
	// bind events here
	var formData = {};
    $("button.btn-maj").on("click", function(event) {
        event.preventDefault();
        // $("form#maj-mon-compte").validate();
        $("form#maj-mon-compte :input.input-with-feedback").each(function(){
            formData[$(this).attr("data-fieldname")] = $(this).val(); // This is the jquery object of the input, do what you will
        });
        frappe.call({
                method: "maj-mon-compte",
                args: formData
            }).then(r => {
                if (r.message) {
                    console.log(r.message);
                    frappe.show_alert({
                        message:__(r.message),
                        indicator:'green'
                    }, 5);
                } else {
                    frappe.throw(__('La mise à jour à échouer, contacter un administrateur.'))
                }
            });
    });
});