from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in hatlab/__init__.py
from hatlab import __version__ as version

setup(
	name='hatlab',
	version=version,
	description='Personalisations de dokos',
	author='Hatlab',
	author_email='infra@hatlab.fr',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
