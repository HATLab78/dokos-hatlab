# CR - Dokos - 8-02-2022

## web-form/inscription-hatlab

Changé message de succès
Reprendre CSS

## server-script/inscription2

Que se passe-t'il si contact existant? Utilisateur créé, mais reremplie le formulaire
- faire en sorte que si l'utilisateur existe il est impossible de le recréer
- implémenter mais manque la redirection qui doit être faite sur le serveur

```
frappe.local.response["type"] = "redirect"
frappe.local.response["location"] = uri
```

! Attention risque cyber si le contact existe !
! Réfléchir, à comment on gère le cas ou le contact existe !

Identifiant unique aléatoire. Si implémenté changement dans le script d'import
dummy, contact_customer = contact.get_link_to_doctype()

Dans le cas ou le contact existe, faire une sauvegarde des informations non mises à jours. frappe.log_error(), frappe.logger.debug()
Case à cocher, contact déjà existant, envoi de message sur télégram en utilisant un incoming webhook.

Intégrer une notif pour chaque nouvelle inscription.

## Préparer un formulaire pour ajouter des enfants pour les parents

Pour utilisateur connecté, avoir un formulaire pour ajouter des enfants.

## Adhésionv3

nom confus pour la page
Intégration stripe ne marche pas

## inscription-hatlab

Mettre un seul numéro fixe ou mobile

Mode de paiement à supprimer

## hl_InscriptionUserOK

Revoir le message et le design

## server-script/Renouvellement Abonnement

Si le client n'existe pas le créer.
Le modèle d'abonnement doit être parfait.

Attention lors de la création de l'utilisateur échec lors de l'association avec le contact.

Portail adhésion ou mail avec lien de paiement.


## hl-inscription

Générer l'abonnement en s'inspirant de Renouvellement Abonnement. Avec subscription.process() pour expédier immédiatement l'email.

tierslieux.dokos.io connection automatique bouton administrateur. 424242424242 numéro de carte dummy.

## Support

Email support qui soit connecté au ticketing Dokos
