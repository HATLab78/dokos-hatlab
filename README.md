## Hatlab

Personalisations de dokos

## Mise en place

Clone initiale
`git clone git@gitlab.com:HATLab78/dokos-hatlab.git`

Clone de dokos pour référence
`git clone --depth 1 git@gitlab.com:dokos/dokos.git`
On ne veut pas suivre son historique donc on ajoute à .gitignore:
`dokos/`


#### License

MIT

## TODO

Correct frappe documentation
Pages and page controller cannot contain - (dash character)


# Shorten Payments
add to cart
url
https://hatpprdokos.hatlab.fr/
payload
item_code=Adhesion+Tarif+Normal&qty=1&with_items=0&cmd=erpnext.shopping_cart.cart.update_cart

Interesting files:
* dokos/erpnext/templates/generators/item/item_add_to_cart.html
* dokos/erpnext/shopping_cart/web_template/item_card_group/item_card_group.html
* dokos/erpnext/templates/includes/macros.html

Strategy to shorten cycle:
* add an option to article document for quick checkout
* create a template for quick checkout probably based on item_card_group.html
* find the right triggers to generate the quote and the bill and the payment link
* test it

# Paiement rapide chd
## Non documenté voir code erpnext
erpnext.shopping_cart.cart.place_order
erpnext.shopping_cart.cart.update_cart
item_code, qty, additional_notes=None, with_items=False, uom=None, booking=None

# Edition document DOKOS
fixtures = [
	{"dt": "Custom Field"}
]
hooks.py
bench export-fixtures