import frappe
from frappe import _


def get_context(context):
    if frappe.session.user=='Guest':
        frappe.throw(_("You need to be logged in to access this page"), frappe.PermissionError)

    context.subscriptions = frappe.get_all("Subscription", filters={"owner": frappe.session.user})
    context.show_sidebar = True

    return context
