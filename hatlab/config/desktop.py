from frappe import _

def get_data():
	return [
		{
			"module_name": "Hatlab",
			"color": "grey",
			"icon": "fa-toolbox",
			"type": "module",
			"label": _("Hatlab")
		}
	]
