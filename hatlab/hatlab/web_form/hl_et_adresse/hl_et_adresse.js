frappe.ready(function () {
	frappe.web_form.after_save = () => {
		var adhesion;

		let d = new frappe.ui.Dialog({
			title: 'Enter details',
			fields: [
				{
					label: 'Type d\'adhésion',
					fieldname: 'adhesion',
					fieldtype: 'Select',
					options: [
						'Adhésion Tarif Normal',
						'Adhésion Tarif Réduit',
						// 'Tarif Libre'
					]
				}
			],
			primary_action_label: 'Submit',
			primary_action(values) {
				adhesion = values;
				console.log(values);
				
				console.log(values.adhesion);
				erpnext.shopping_cart.update_cart({
					item_code: adhesion.adhesion,
					qty: 1
				}).then(() => {
					frappe.call({
						method: "erpnext.shopping_cart.cart.place_order",
						callback: function(r) {
							if (r.exc) {
								var msg = "";
								if (r._server_messages) {
									msg = JSON.parse(r._server_messages || []).join("<br>");
								}

								frappe.msgprint(msg || __("Something went wrong!"))
								setTimeout(() => {
									window.location.href = "/cart";
								}, 5000);
							} else {
								window.location.href = '/orders/' + encodeURIComponent(r.message);
							}
						}
					});
				})
				d.hide();
			}
		});
		
		d.show();
		//TODO: mettre le bon article
	}
})
