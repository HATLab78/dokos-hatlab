import frappe

def contact_after_insert(doc, method):
	if doc.user:
		redirect_url = f"hl-contact?name={doc.name}"
		frappe.db.set_value("User", doc.user, "redirect_url", redirect_url)

def customer_after_insert(doc, method):
	user = frappe.session.user
	address_names = frappe.get_all("Address", filters={"owner": user}, pluck="name")

	for address_name in address_names:
		address = frappe.get_doc("Address", address_name)
		if not address.links:
			address.append('links', dict(link_doctype='Customer', link_name=doc.name))
			address.flags.ignore_mandatory = True
			address.flags.ignore_permissions = True
			address.save()